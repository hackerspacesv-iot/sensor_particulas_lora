/*******************************************************************************
   Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
   Copyright (c) 2018 Terry Moore, MCCI
   Copyright (c) 2019 Mario Gomez, Hackerspace San Salvador.

   Permission is hereby granted, free of charge, to anyone
   obtaining a copy of this document and accompanying files,
   to do whatever they want with them without any restriction,
   including, but not limited to, copying, modification and redistribution.
   NO WARRANTY OF ANY KIND IS PROVIDED.

   This example sends a measurement of particulate matter concentration
   to The Things Network. The data is collected with the Sensirion SPS30.
   Fields are encoded as fixed-point float numbers due limitations on
   Javascript conversion at The Things Network Platform.

   This uses ABP (Activation-by-personalisation), where a DevAddr and
   Session keys are preconfigured (unlike OTAA, where a DevEUI and
   application key is configured, while the DevAddr and session keys are
   assigned/generated in the over-the-air-activation procedure).

   To use this sketch, first register your application and device with
   the things network, to set or generate a DevAddr, NwkSKey and
   AppSKey. Each device should have their own unique values for these
   fields.

   Do not forget to define the radio type correctly in
   arduino-lmic/project_config/lmic_project_config.h or from your BOARDS.txt.

   More info about this Sketch and connections is available over this
   repository:
     https://gitlab.com/hackerspacesv-iot/sensor_particulas_lora

   This Sketch also uses software from Sensirion. Their respective licenses
   are available on the header of each file. The source code included on
   this Sketch is available over:
     https://github.com/Sensirion/embedded-uart-sps.git

   Requires Arduino-LMIC from MCCI-Catena
     https://github.com/mcci-catena/arduino-lmic

 *******************************************************************************/
// Include the UART library from sensirion.
#include "sensirion_uart.h"
// You must define the float32_t implementation available for your platform.
// Default implementation works well with Arduino.
#include "sensirion_arch_config.h"
#include "sps30.h"
#include "ttn_config.h"

#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

#ifdef PLATFORM_ARDUINO_UNO
// TODO: Figure out why this Sketch doesn't work if you don't include and
//       initialize a SoftwareSerial instance. Could be related to the
//       incompatibilities between interrupt handling in LMIC. 
#include <SoftwareSerial.h>
SoftwareSerial DEBUG_PORT(DEBUG_PORT_RX_PIN, DEBUG_PORT_TX_PIN);
#endif

// LoRaWAN NwkSKey, network session key
// This should be in big-endian (aka msb).
static const PROGMEM u1_t NWKSKEY[16] = TTN_NWKSKEY;

// LoRaWAN AppSKey, application session key
// This should also be in big-endian (aka msb).
static const u1_t PROGMEM APPSKEY[16] = TTN_APPSKEY;

// LoRaWAN end-device address (DevAddr)
// See http://thethingsnetwork.org/wiki/AddressSpace
// The library converts the address to network byte order as needed, so this should be in big-endian (aka msb) too.
static const u4_t DEVADDR = TTN_DEVADDR; // <-- Change this address for every node!

// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in arduino-lmic/project_config/lmic_project_config.h,
// otherwise the linker will complain).
void os_getArtEui (u1_t* buf) { }
void os_getDevEui (u1_t* buf) { }
void os_getDevKey (u1_t* buf) { }

uint8_t mydata[] = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
static osjob_t sendjob;

// This example was written for use in the AU915 TTN Region with SF7BW125 coding 4/5.
// To keep the airtime under the fair-use we can send a message around every 12
// minutes.
// To calculate the maximum number of messages see:
//    https://avbentem.github.io/lorawan-airtime-ui/ttn/au915/18
// We send a measurement every 12 minutes to comply with the air-time regulations.
#ifdef TTN_DEBUG
const unsigned TX_INTERVAL = 10;
#else
const unsigned TX_INTERVAL = MSG_INTERVAL;
#endif

// Pin mapping for the Huzzaa ESP8266 (ESP-12 based)
#ifdef PLATFORM_ESP8266
const lmic_pinmap lmic_pins = {
  .nss = 4,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 5,
  .dio = {16, 0, LMIC_UNUSED_PIN}
};
#endif

// Pin mapping for the Teensy 3.2
#ifdef PLATFORM_TEENSY32
const lmic_pinmap lmic_pins = {
  .nss = 10,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 9,
  .dio = {14, 15, 16}
};
#endif

// Pin mapping for Arduino UNO
#ifdef PLATFORM_ARDUINO_UNO
const lmic_pinmap lmic_pins = {
  .nss = 10,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = 9,
  .dio = {6, 7, 8}
};
#endif

void onEvent (ev_t ev) {
  switch (ev) {
    case EV_SCAN_TIMEOUT:
      break;
    case EV_BEACON_FOUND:
      break;
    case EV_BEACON_MISSED:
      break;
    case EV_BEACON_TRACKED:
      break;
    case EV_JOINING:
      break;
    case EV_JOINED:
      break;
    case EV_JOIN_FAILED:
      break;
    case EV_REJOIN_FAILED:
      break;
    case EV_TXCOMPLETE:
#ifdef TTN_DEBUG
        DEBUG_PORT.println(F("Transmission complete (including 2nd RX window)."));
#endif
      if (LMIC.txrxFlags & TXRX_ACK) {
#ifdef TTN_DEBUG
        DEBUG_PORT.println(F("ACK Received..."));
#endif
      }
      if (LMIC.dataLen) {
#ifdef TTN_DEBUG
        DEBUG_PORT.println(F("Data received on 2nd RX Window..."));
#endif
      }
#ifdef TTN_DEBUG
    DEBUG_PORT.print(F("Next try re-scheduled "));
    DEBUG_PORT.print(TX_INTERVAL, DEC);
    DEBUG_PORT.println(F(" from now."));
#endif
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
      break;
    case EV_LOST_TSYNC:
      break;
    case EV_RESET:
      break;
    case EV_RXCOMPLETE:
      break;
    case EV_LINK_DEAD:
      break;
    case EV_LINK_ALIVE:
      break;
    /*
      || This event is defined but not used in the code. No
      || point in wasting codespace on it.
      ||
      || case EV_SCAN_FOUND:
      ||    Serial.println(F("EV_SCAN_FOUND"));
      ||    break;
    */
    case EV_TXSTART:
      break;
    default:
      break;
  }
}

void do_send(osjob_t* j) {
  uint8_t msg_type = 0;
  struct sps30_measurement measurement;
  uint32_t pm1p0;
  uint32_t pm2p5;
  uint32_t pm4p0;
  uint32_t pm10p0;
  int16_t ret;

#ifdef TTN_DEBUG
  DEBUG_PORT.println(F("Trying to start measurement..."));
#endif
  ret = sps30_start_measurement();
  if (ret != 0) {
#ifdef TTN_DEBUG
  DEBUG_PORT.println(F("Unable to start measurement..."));
#endif
    goto schedule_next_call;
  }

#ifdef TTN_DEBUG
  DEBUG_PORT.println(F("Waiting 10 seconds to allow flow stabilization..."));
#endif
  delay(10000); // Wait before measure

#ifdef TTN_DEBUG
  DEBUG_PORT.println(F("Checking if radio is free..."));
#endif
  if (LMIC.opmode & OP_TXRXPEND) {
#ifdef TTN_DEBUG
    DEBUG_PORT.println(F("Radio not ready. Stopping measurement."));
#endif
    goto stop_measurement;
  }

#ifdef TTN_DEBUG
  DEBUG_PORT.println(F("Trying to read measurement..."));
#endif
  ret = sps30_read_measurement(&measurement);
  if (ret < 0) {
#ifdef TTN_DEBUG
    DEBUG_PORT.println(F("Unable to read measurement. Stopping measurement."));
#endif
    goto stop_measurement;
  }

  if (SPS_IS_ERR_STATE(ret)) {
    mydata[17] = 1;
#ifdef TTN_DEBUG
    DEBUG_PORT.println(F("Data read sucessfully, but could not be accurate..."));
#endif
  } else {
    mydata[17] = 0;
  }
#ifdef DEBUG_LED
  digitalWrite(DEBUG_LED, HIGH); // Just a little indication to know is working
#endif

  // Transform and copy values to the buffer
  pm1p0 = (uint32_t)(measurement.mc_1p0 * 100000.0);
  pm2p5 = (uint32_t)(measurement.mc_2p5 * 100000.0);
  pm4p0 = (uint32_t)(measurement.mc_4p0 * 100000.0);
  pm10p0 = (uint32_t)(measurement.mc_10p0 * 100000.0);
  memcpy(mydata, &msg_type, sizeof(msg_type));
  memcpy(mydata + sizeof(msg_type), &pm1p0, sizeof(float32_t));
  memcpy(mydata + sizeof(msg_type) + sizeof(float32_t) * 1, &pm2p5, sizeof(float32_t));
  memcpy(mydata + sizeof(msg_type) + sizeof(float32_t) * 2, &pm4p0, sizeof(float32_t));
  memcpy(mydata + sizeof(msg_type) + sizeof(float32_t) * 3, &pm10p0, sizeof(float32_t));

#ifdef TTN_DEBUG
    DEBUG_PORT.println(F("Sending LoRaWAN Message..."));
#endif
  // Send data
  LMIC_setTxData2(1, mydata, sizeof(mydata) - 1, 0);
#ifdef DEBUG_LED
  digitalWrite(DEBUG_LED, LOW); // Just a little indication to know is working
#endif

stop_measurement:
  sps30_stop_measurement();

schedule_next_call:
  // Note: We allways set the new callback, but it could be re-scheduled after a 
  // successfull LoRaWAN transmissión is finished.
#ifdef TTN_DEBUG
    DEBUG_PORT.print(F("Next try is scheduled "));
    DEBUG_PORT.print(TX_INTERVAL, DEC);
    DEBUG_PORT.println(F(" from now."));
#endif
  os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
}

void setup() {

  // Enable debug LED
#ifdef DEBUG_LED
  pinMode(DEBUG_LED, OUTPUT);
#endif

#if defined(PLATFORM_ARDUINO_UNO) || defined(PLATFORM_ESP8266) 
  delay(10000); // This delay is to allow to connect the sensor on first boot.
#endif

#ifdef TTN_DEBUG
  DEBUG_PORT.begin(DEBUG_PORT_SPEED);
  DEBUG_PORT.println(F("Debug enabled... Starting up..."));
#endif

  //delay(10000);

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Set static session parameters. Instead of dynamically establishing a session
  // by joining the network, precomputed session parameters are be provided.
#ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession (0x13, DEVADDR, nwkskey, appskey);
#else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession (0x13, DEVADDR, NWKSKEY, APPSKEY);
#endif

#if defined(CFG_eu868)
  // Set up the channels used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B), BAND_CENTI);      // g-band
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),  BAND_CENTI);      // g-band
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK,  DR_FSK),  BAND_MILLI);      // g2-band
  // TTN defines an additional channel at 869.525Mhz using SF9 for class B
  // devices' ping slots. LMIC does not have an easy way to define set this
  // frequency and support for class B is spotty and untested, so this
  // frequency is not configured here
#elif defined(CFG_us915)
  // NA-US channels 0-71 are configured automatically
  // but only one group of 8 should (a subband) should be active
  // TTN recommends the second sub band, 1 in a zero based count.
  // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
  LMIC_selectSubBand(1);
#elif defined(CFG_au921)
  // AU915 channels 0-71 are configured automatically
  // but only one group of 8 should (a subband) should be active
  // TTN recommends the second sub band, 1 in a zero based count.
  // https://github.com/TheThingsNetwork/gateway-conf/blob/master/AU-global_conf.json
  LMIC_selectSubBand(1);
#endif

  // Disable link check validation
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = DR_SF9;

  // Set data rate and transmit power for uplink
  LMIC_setDrTxpow(DR_SF7, 14);

  // Start job
#ifdef TTN_DEBUG
  DEBUG_PORT.println(F("LMIC warm up finished..."));
  DEBUG_PORT.println(F("Sending first message..."));
#endif
  sensirion_uart_open();
  do_send(&sendjob);
}

void loop() {
  os_runloop_once();
}
