#ifndef TTN_CONFIG_H
#define TTN_CONFIG_H

/*
 * Utiliza este archivo de configuración para definir las credenciales de conección a The Things Network.
 * Este ejemplo solo soporta el método de autenticación ABP. Copia los valores requeridos desde la consola
 * de The Things Network
 */

// Copia el valor de Network Sessión Key en "FILL_ME_IN"
#define TTN_NWKSKEY  FILL_ME_IN
// Copia el valor de Application Sessión Key en "FILL_ME_IN"
#define TTN_APPKEY   FILL_ME_IN
// Copia el valor de Device Address en "FILL_ME_IN" anteponiendo 0x
#define TTN_DEVADDR  0xFILL_ME_IN

// Establece el intérvalo de envío de mensajes (en segundos)
#define MSG_INTERVAL 60*12

// Uncomment the following line
// This also causes to generate messages every 10 seconds.
// Please not use in deployment
// Check also "sensirion_arch_config.h" to select the coresponding board
//#define TTN_DEBUG

#endif
