# Sensor de Partículas para The Things Network (LoRaWAN)

Este proyecto contiene el código fuente y documentación para la creación de un
sensor inalámbrico de materia particulada suspendida en el aire para PM1.0,
PM2.5, PM4.0 y PM10.0.

[Here you can find instructions in English](./README.en.md)

## Tarjetas electrónicas soportadas

 - Teensy 3.1/3.2
 - Arduino UNO*
 - Adafruit HUZZAH ESP8266 o ESP-12
 
**Arduino UNO**: En la tarjeta Arduino UNO este Sketch ocupa casi toda la
memoria del microcontrolador esto significa que agregar funcionalidad adicional
podría ser complicado. Solo el puerto serie utilizado para programar puede
soportar la velocidad a la cual funciona el Sensirion SPS30. Esto significa
que es necesario desconectar el sensor mientras se programa y conectarlo cuando
se requiera operación normal. Al habilitar la depuración se utiliza un puerto
SoftSerial disponible en los pines 2 (RXIN) y 3 (TXOUT) y solo pueden mostrarse
los mensajes de depuración conectando un convertidor USB-Serie externo.

**Adafruit HUZZAH ESP8266 o ESP-12**: En la tarjeta basada en el ESP8266 se
encuentra un problema similar al UNO ya que el puerto serie principal es
utilizado por el sensor de partículas. Por lo que no se puede depurar
directamente desde USB. Para depurar esta tarjeta es necesario
utilizar un convertidor USB-Serie externo conectado al pin 0 (TXOUT).

## Materiales
Para construir este proyecto requerirás al menos de los siguientes materiales:

 - Alguna de las siguientes tarjetas programables:

   - [Teensy 3.1 o 3.2](https://www.pjrc.com/store/teensy32.html) 

   - [Arduino UNO](https://store.arduino.cc/usa/arduino-uno-rev3/). Si utilizas un Arduino UNO requerirás también dos [convertidor de nivel de sparkfun](https://www.sparkfun.com/products/12009) 

 o dos [TBX0104](https://www.sparkfun.com/products/11771).

   - [Adafruit HUZZAH o un ESP12](https://store.arduino.cc/usa/arduino-uno-rev3/). Si utilizas esta tarjeta requerirás también un [convertidor de nivel de sparkfun](https://www.sparkfun.com/products/12009) o un [TBX0104](https://www.sparkfun.com/products/11771).

 - Radio [LoRa RFM95W](https://www.adafruit.com/product/3072)

 - Sensor de partículas [Sensirion SPS30](https://www.sparkfun.com/products/15103)

## Requerimientos de Software

 - Una versión reciente del [Arduino IDE](https://www.arduino.cc/en/Main/Software)
 - Librería [Arduino-LMIC de MCCI-Catena](ttps://github.com/mcci-catena/arduino-lmic)
 
### Instrucciones de Configuración de Arduino-LMIC

Instala la librería Arduino-LMIC en la siguiente carpeta:

Linux:

    $HOME/Arduino/libraries

Windows:

    C:\Users\<Nombre de Usuario>\Documents\Arduino\libraries

La librería Arduino-LMIC requiere que se establezca la configuración regional
para LoRaWAN. Esto se hace cambiando el archivo ***lmic_project_config.h*** que
se
 encuentra en la siguiente carpeta:

Linux:

    $HOME/Arduino/libraries/arduino-lmic/project_config

Windows:

    C:\Users\<Nombre de Usuario>\Documents\Arduino\libraries\arduino-lmic\project_config

Quita el comentario de la línea que corresponde a la región LoRaWAN. Para El
Salvador
 la región corresponde a *Australía 915 (CFG_au291)*.

    // project-specific definitions
    //#define CFG_eu868 1
    //#define CFG_us915 1
    #define CFG_au921 1
    //#define CFG_as923 1
    // #define LMIC_COUNTRY_CODE LMIC_COUNTRY_CODE_JP       /* for as923-JP */
    //#define CFG_in866 1
    #define CFG_sx1276_radio 1
    //#define LMIC_USE_INTERRUPTS

## Configuración de la Tableta

La tableta será automáticamente detectada al momento de compilar. Aunque si quieres
revisar la configuración por tableta o agregar soporte para una tarjeta diferente
revisa el archivo "sensirion_arch_config.h".

**Nota:** Por el momento solo las plataformas Teensy 3.1/3.2, Arduino UNO y
ESP12 están soportadas.

## Configuración del Sketch
Abre la pestaña "ttn_config_example.h" y rellena los valores que se solicitan
con la información
 de tu dispositivo que obtienes desde la [Consola de The Things Network](https://console.thethingsnetwork.org/).

**Importante:** Asegúrate de que en la consola la forma de activación sea
**ABP** y no tengas marcada
 la opción **Frame Counter Checks** en las opciones
de configuración.

Copia los parámetros según se indica reemplazando **FILL_ME_IN** por el valor
obtenido de
 la consola de TTN:

    // Copia el valor de Network Sessión Key en "FILL_ME_IN"
    #define TTN_NWKSKEY  FILL_ME_IN
    // Copia el valor de Application Sessión Key en "FILL_ME_IN"
    #define TTN_APPKEY   FILL_ME_IN
    // Copia el valor de Device Address en "FILL_ME_IN" anteponiendo 0x
    #define TTN_DEVADDR  0xFILL_ME_IN

Cambia la frecuencia de envío de mensajes en la siguiente variable:

    // Establece el intérvalo de envío de mensajes
    #define MSG_INTERVAL 60*12*1000

**Importante**: Se espera que los usuarios de The Things Network respeten el
límite de mensajes
 (tiempo de aire) permitido para la región. En este
ejemplo se intenta enviar un mensaje cada 12 minutos. Para estimar cuántos
mensajes deberían ser enviados como
 máximo por día, se puede consultar
el siguiente sitio web:

https://avbentem.github.io/lorawan-airtime-ui/ttn/eu868

Ingresa en la calculadora un *tamaño de mensaje de 18 bytes* y selecciona la
región LoRaWAN que
 aplique para tu país.

### Configuración de Depuración
Cuando el Sketch está configurado en modo depuración se envían mensajes de
estado al puerto Serie.
 Esto resulta muy útil para diagnosticar. El intérvalo
de mensajes también se reduce a sólo 10 segundos.
 No habilites la depuración en
dispositivos finales ya que podrías infringir las reglas locales de acceso
a redes LoRaWAN.

To enable the debug mode un-comment the following line

    //#define TTN_DEBUG

### Configuración del formato de datos en The Things Network
Utiliza el siguiente decodificador para extraer los datos de los mensajes
enviados por el sensor. Este código se agrega en la pestaña correspondiente
de la aplicación LoRaWAN en la consola de The Things Network:

    function byte_array_to_int(byte_array) {
      var value = (byte_array[3]<<24) | (byte_array[2]<<16) | (byte_array[1]<<8) | byte_array[0]
      return value
    }

    function Decoder(bytes, port) {
      // Decode an uplink message from a buffer
      // (array) of bytes to an object of fields.
      var decoded = {};
    
      // if (port === 1) decoded.led = bytes[0];
      if(bytes.length==18) {
        if(bytes[0]===0) {
          decoded.pm1p0 = byte_array_to_int([bytes[1],bytes[2],bytes[3],bytes[4]])/100000.0;
          decoded.pm2p5 = byte_array_to_int([bytes[5],bytes[6],bytes[7],bytes[8]])/100000.0;
          decoded.pm4p0 = byte_array_to_int([bytes[9],bytes[10],bytes[11],bytes[12]])/100000.0;
          decoded.pm10p0 = byte_array_to_int([bytes[13],bytes[14],bytes[15],bytes[16]])/100000.0;
          decoded.accurate = (bytes[17]===0) ? true : false;
        }
      }
    
      return decoded;
    }

Puedes probar si el decodificador funciona ingresando el siguiente valor:

    0032AF11000F2718005F241D00172D1F0000

Deberías obtener un resultado como el siguiente:


    {
      "accurate": true,
      "pm1p0": 11.58962,
      "pm10p0": 20.43159,
      "pm2p5": 15.82863,
      "pm4p0": 19.09855
    }

## Conexiones
Conecta el radio RFM95W de la siguiente manera:

**Teensy 3.1/3.2**

| Teensy | RFM95W |
|--------|--------|
|  3.3V  |  3.3V  |
|  GND   |  GND   |
|  9     |  RESET |
|  10    |  NSS   |
|  11    |  MOSI  |
|  12    |  MISO  |
|  13    |  SCK   |
|  14    |  DIO0  |
|  15    |  DIO1  |
|  16    |  DIO2  |

**Arduino UNO**

En Arduino UNO se requiere un convertidor de nivel lógico para el RFM95W. El
convertidor debe soportar niveles de 3.3V y conversión a 5V.

|  UNO   |  Conv 5V   |  Conv 3V3  | RFM95W |
|--------|------------|------------|--------|
|  5V    |  5V        |  NC        | NC     |
|  3.3V  |  NC        |  3V3       | 3V3    |
|  GND   |  GND       |  GND       | GND    |
|  9     |  IOHV1     |  IOLV1     | RESET  |
|  10    |  IOHV2     |  IOLV2     | NSS    |
|  12    |  IOHV3     |  IOLV3     | MOSI   |
|  11    |  IOHV4     |  IOLV4     | MISO   |
|  13    |  IOHV5     |  IOLV5     | SCK    |
|  6     |  IOHV6     |  IOLV6     | DIO0   |
|  7     |  IOHV7     |  IOLV7     | DIO1   |
|  8     |  IOHV8     |  IOLV8     | DIO2   |

Puedes usar el [convertidor de nivel de sparkfun](https://www.sparkfun.com/products/12009) o el [TBX0104](https://www.sparkfun.com/products/11771)
como convertidor de nivel de voltaje.

**Adafruit HUZZAH o ESP-12**

| ESP12  | RFM95W |
|--------|--------|
|  3.3V  |  3.3V  |
|  GND   |  GND   |
|  5     |  RESET |
|  4     |  NSS   |
|  13    |  MOSI  |
|  12    |  MISO  |
|  14    |  SCK   |
|  16    |  DIO0  |
|  0     |  DIO1  |

DIO2 no está conectado en el ESP-12. No hay suficientes pines.

Conecta el Sensirion SPS30 de la siguiente manera:

**Teensy 3.1/3.2**

| Teensy | Sensirion SPS30 |
|--------|-----------------|
|  Vin   |       VDD       |
|  GND   |       GND       |
|  0     |       TX        |
|  1     |       RX        |
|  N/C   |       SEL       |

**Arduino UNO**

| UNO    | Sensirion SPS30 |
|--------|-----------------|
|  5V    |       VDD       |
|  GND   |       GND       |
|  0     |       TX        |
|  1     |       RX        |
|  N/C   |       SEL       |

En el Arduino UNO deberás desconectar el sensor cada vez que programes
la tableta electrónica.

**Adafruit HUZZAH o ESP-12**

En ESP-12 se requiere un convertidor de nivel lógico para el Sensirion
SPS30. El convertidor debe soportar niveles de 3.3V y conversión a 5V.

|  UNO   |  Conv 5V   |  Conv 5V   | Sensirion SPS30 |
|--------|------------|------------|-----------------|
|  5V    |  5V        |  NC        |       NC        |
|  3.3V  |  NC        |  3V3       |       3V3       |
|  GND   |  GND       |  GND       |       GND       |
|  1     |  IOLV1     |  IOHV1     |       TX        |
|  3     |  IOLV2     |  IOHV2     |       RX        |

**Nota:** Recuerda que en el ESP32 el convertidor de nivel utiliza el
convertidor de nivel para conectarse al Sensirion SPS30. Al igual que el UNO
deberás de desconectar el Sensirion SPS30 cada vez que cargues el Sketch.

## Copyright
Este Sketch ha sido elaborado por Mario Gómez del Hackerspace San Salvador y
utiliza diferentes librerías y componentes de terceros. Para información sobre
licencias por favor verificar los encabezados incluídos en cada archivo de
código fuente.
